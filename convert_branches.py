#!/usr/bin/env python

# Based on http://askubuntu.com/a/262921/22375

import os
import sys
from subprocess import call, check_call, CalledProcessError
from launchpadlib.launchpad import Launchpad


def list_branches(project):
    cachedir = os.path.expanduser("~/.launchpadlib/cache/")
    launchpad = Launchpad.login_anonymously('find_branches',
                                            'production',
                                            cachedir)
    try:
        lpproject = launchpad.projects[project]
        return lpproject.getBranches()
    except KeyError:
        print "Project unknown... \nUsage: " + sys.argv[0] + " lp_project_name"


def convert_branches(project, bzrdir, gitdir, bzrmarks=None, gitmarks=None):
    bzrmarks = bzrmarks or project + '.marks.bzr'
    gitmarks = gitmarks or project + '.marks.git'
    for b in list_branches(project):
        if b.lifecycle_status not in ["Abandoned", "Merged"]:
            print "Converting branch", b.bzr_identity
            name = b.bzr_identity.split(':')[-1]
            if name == project:
                continue
            if name.startswith('~'):
                name = '/'.join(name[1:].split('/')[0::2])
            else:
                name = name.split('/')[-1]
            try:
                check_call(['bzr', 'switch', b.bzr_identity], cwd=bzrdir)
            except CalledProcessError:
                print "Switching to branch", b.bzr_identity, "failed"
                # Branch may have become unbound: try to bind and ignore error
                call(['bzr', 'bind'], cwd=bzrdir)
                continue
            try:
                check_call(' '.join(['bzr',
                                     'fast-export',
                                     '--plain',
                                     '--git-branch=' + name,
                                     '--marks=' + bzrmarks,
                                     bzrdir,
                                     '|',
                                     'git',
                                     '--git-dir=' + gitdir,
                                     'fast-import',
                                     '--import-marks=' + gitmarks,
                                     '--export-marks=' + gitmarks]),
                           shell=True)
            except CalledProcessError:
                print "Importing branch", b.bzr_identity, "failed"

if __name__ == '__main__':
    convert_branches(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
