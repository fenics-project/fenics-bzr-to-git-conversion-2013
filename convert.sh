#!/bin/bash
#
# Script for converting the FEniCS Launchpad repositories to Git
#
# Created by Florian Rathgeber, Anders Logg and Garth Wells
# 2013-04-05

# List of repositories to convert
if [ -z "$@" ]; then
  projects="dolfin dorsal ferari ffc fiat instant ufc ufl fenics-web fenics-book"
else
  projects="$@"
fi

# Get root directory
root="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Create directory structure
bzr_dir=$root/repositories/bzr
git_dir=$root/repositories/git
marks_dir=$root/marks
mkdir -p $bzr_dir
mkdir -p $git_dir
mkdir -p $marks_dir

echo Starting conversion of ${projects} at `date`
echo

# Convert all projects
for p in ${projects}; do

  echo
  echo ===== Converting ${p} =====
  echo

  # Branch trunk repository from Launchpad
  bzr checkout lp:$p $bzr_dir/$p.bzr

  # Create git repository
  git init --bare $git_dir/$p.git
  repo_to_push=$git_dir/$p.git

  # Convert from Launchpad trunk bzr repo into git repo
  bzr fast-export --plain --export-marks=$marks_dir/${p}.marks.bzr $bzr_dir/$p.bzr | \
    git --git-dir=$git_dir/$p.git fast-import --export-marks=$marks_dir/${p}.marks.git

  # Convert branches
  $root/convert_branches.py ${p} $bzr_dir/$p.bzr $git_dir/$p.git $marks_dir/${p}.marks.bzr $marks_dir/${p}.marks.git

  # Filter files if we find a list of files to strip
  echo "Size before filtering: "`du -sh $git_dir/$p.git | awk '{print $1}'`
  if [ -e $root/${p}_cleanup/files_to_strip.txt ]; then

      # Remove hidden file which may be a remnant for a previous
      # failed filtering attempt, in which case the subsequent
      # filtering won't work
      rm -rf $root/.git-rewrite

      # Filter in new clone so we can archive unfiltered repositories
      git clone --mirror $git_dir/$p.git $git_dir/$p.filtered.git
      repo_to_push=$git_dir/$p.filtered.git

      # Get the list of files to strip (easier to copy than to handle bash quoting)
      cp $root/${p}_cleanup/files_to_strip.txt /tmp/files_to_strip.txt

      # Strip files
      git --git-dir=$git_dir/$p.filtered.git filter-branch --index-filter \
        'git rm -rf --cached --ignore-unmatch $(cat /tmp/files_to_strip.txt)' \
        --tag-name-filter cat -- --all > $p.filter.log

      # Delete references to non-filtered branches
      git --git-dir=$git_dir/$p.filtered.git for-each-ref --format="%(refname)" 'refs/original' | \
        xargs -n 1 git --git-dir=$git_dir/$p.filtered.git update-ref -d
      echo "Size after filtering: "`du -sh $git_dir/$p.filtered.git | awk '{print $1}'`
      echo

      # Repack files
      git --git-dir=$git_dir/$p.filtered.git reflog expire --expire=now --all
      git --git-dir=$git_dir/$p.filtered.git gc --aggressive --prune=now
      git --git-dir=$git_dir/$p.filtered.git repack -ad
      echo "Size after filtering + repacking: "`du -sh $git_dir/$p.filtered.git | awk '{print $1}'`
      echo

  fi

  # Repack files
  git --git-dir=$git_dir/$p.git reflog expire --expire=now --all
  git --git-dir=$git_dir/$p.git gc --aggressive --prune=now
  git --git-dir=$git_dir/$p.git repack -ad
  echo
  echo "Size after repacking: "`du -sh $git_dir/$p.git | awk '{print $1}'`

  # Push master branch to BitBucket (this will fail if the filtered repo has
  # already been pushed, but that's what we want)
  echo
  echo "Pushing master branch to Bitbucket"
  git --git-dir=$repo_to_push remote add bitbucket git@bitbucket.org:fenics-project/$p.git
  git --git-dir=$repo_to_push push bitbucket master

done

echo
echo Finished conversion of ${projects} at `date`
