This repository documents the conversion of the FEniCS code repositories
from Bazaar (hosted on Launchpad) to Git (hosted on Bitbucket) 2013-04-05.

The conversion includes the following repositories:

  dolfin
  dorsal
  ferari
  ffc
  fiat
  instant
  ufc
  ufl
  fenics-web
  fenics-book

The conversion was done using the script convert.sh.

That script does the following for each repository:

1. Convert from bzr to git

2. Convert and import all feature branches on Launchpad

3. Clean out certain files (big files) from the histories or DOLFIN and FFC

4. Push the new repository to Bitbucket (only master branches)

After completion of the script, the following steps were taken manually:

5. Archive this repository (containing *everything*) to both Bitbucket
   and the FEniCS web server

Note to self: run the conversion script with the following commands:

  unset LANG
  time ./convert.sh | tee conversion.log
